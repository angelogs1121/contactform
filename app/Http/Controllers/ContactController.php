<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contact;

class ContactController extends Controller
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email',
            'comments'      => 'required|max:100',
            'captcha'       => 'required|captcha'
        ],
        [
            'captcha'       => 'The Captcha code you entered was invalid'
        ]);
    }

    /**
     * Get visitor address via API
     *
     * @param mixed $ip IP Address of the user. By default `null` for getting the current IP address
     * @return array Address of the user from the API.
     */
    public function getVisitorAddress($ip = null)
    {
        return json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
    }

    /**
     * Save the record subbmit by the visitor
     *
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function submit(Request $request)
    {
        $this->validator($request->all())->validate();

        $visitorAddress = $this->getVisitorAddress();

        $request->request->add([
            'country'   => $visitorAddress->country,
            'city'      => $visitorAddress->city
        ]);

        Contact::create($request->all());

        return  response(['success' => true, 'message' => 'Thank you for your contacting us. Have a nice day.'], 200)
                ->header('Content-type', 'application/json');
    }
}
