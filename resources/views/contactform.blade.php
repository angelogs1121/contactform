<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Contact Form</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
        .form-horizontal{
            display:block;
            width:50%;
            margin:0 auto;
        }

        h2.form-title {
            text-align: center;
            margin-bottom: 50px;
        }

        .refresh-captcha-btn {
            cursor: pointer;
            color: #337ab7;
        }
    </style>

</head>
<body>
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" id="form">

                    <div class="alerts"></div>

                    <h2 class="form-title">Online Contact Form</h2>

                    <div class="form-group">
                        <label for="first-name" class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="first-name" placeholder="First Name" name="first_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last-name" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="last-name" placeholder="Last Name" name="last_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email Address</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" placeholder="Email Address" name="email" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comments" class="col-sm-3 control-label">Comments</label>
                        <div class="col-sm-9">
                            <textarea id="comments" class="form-control" rows="5" placeholder="Write your comments here..." name="comments" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <div id="refresh-captcha">
                                {!! captcha_img() !!}
                            </div>
                            <span class="refresh-captcha-btn">Refresh</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <input type="text" id="captcha" class="form-control" name="captcha" placeholder="Enter the code above here" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function() {

            let refreshCaptcha = function () {
                $.ajax({
                    type: 'get',
                    url: '/refresh-captcha',
                    dataType: 'html',
                    success: function (data) {
                        $('#refresh-captcha').html(data);
                    },
                    error: function (err) {
                        console.log(err.resopnseText);
                    }
                });
            }

            $('.refresh-captcha-btn').click(function () {
                refreshCaptcha();
            });

            $('#form').submit(function (e) {
                e.preventDefault();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: '/api/submit',
                    dataType: 'json',
                    data: $('#form').serialize(),
                    success: function (data) {

                        $fixture = this.$fixture = $([
                            "<div class='alert alert-success'>",
                                "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>",
                                "<strong class='success'>Success!</strong>",
                                "<span>" + data.message + "</span>",
                            "</div>"
                        ].join("\n"));

                        $('.alerts').html($fixture);
                        $('#form')[0].reset();
                    },
                    error: function (err) {
                        let error = err.responseJSON;
                        let errMsgs = [
                            "<div class='alert alert-danger'>",
                            "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
                        ];

                        $.each(error, function (i, v) {
                            console.log(v[0]);
                            errMsgs.push('<p>' + v[0] + '</p>');
                        });

                        errMsgs.push("<span></span>", "</div>");

                        $fixture = this.$fixture = $(errMsgs.join("\n"));

                        $('.alerts').html($fixture);

                        console.log(error);
                        refreshCaptcha();
                    }
                });
            });

        });
    </script>
</body>
</html>